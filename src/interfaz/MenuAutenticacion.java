/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz;

import java.util.*;
import static interfaz.Sistema.usuarioActivo;

/**
 *
 * @author Brank
 */
public class MenuAutenticacion extends Menu {

    public MenuAutenticacion(ArrayList<String> opciones) {
        super(opciones);
    }

    @Override
    public void cargarMenu() {
        boolean salir = false;
        while (!salir) {
            mostrarMenu();
            System.out.print("Ingrese opción : ");
            String opcion = Util.ingresoString();
            while ((!(Util.isNumeric(opcion))) || (!(Util.isBetween(1, 3, opcion)))) {
                System.out.print("Opción incorrecta. Ingrese nuevamente : ");
                opcion = Util.ingresoString();
            }
            switch (opcion) {
                case "1":
                    Menu menuPersona = usuarios.Persona.iniciarSesion();                    
                    if (!(menuPersona == null)) {
                        menuPersona.cargarMenu();
                    }
                    break;
                case "2":
                    usuarios.Tarjetahabiente.registrarUsuario();
                    break;
                case "3":
                    salir = salir();
                    break;
            }
        }
    }

    public static ArrayList<String> añadirOpciones() {
        ArrayList<String> opciones = new ArrayList<>();
        opciones.add("Iniciar sesión");
        opciones.add("Registrarse");
        opciones.add("Salir");
        return opciones;
    }

    protected boolean salir() {
        System.out.print("¿Está seguro de que desea salir de la aplicación? S/N : ");
        String salir = Util.ingresoString();
        while (!(salir.equalsIgnoreCase("s"))) {
            switch (salir) {
                case "n":
                    return false;
                case "N":
                    return false;
                default:
                    System.out.print("¿Está seguro de que desea salir de la aplicación? S/N : ");
                    salir = Util.ingresoString();
            }
        }
        return true;
    }
}
